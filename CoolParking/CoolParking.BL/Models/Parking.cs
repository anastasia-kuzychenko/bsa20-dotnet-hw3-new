﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

//   Паркінг містить Баланс та колекцію Тр.засобів.Об'єкт цього класу повинен бути лише один на всю програму (патерн Singleton).
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
		private static Parking parking;
		public decimal Balance { get; set; }
		public List<Vehicle> Vehicles { get; set; }
		private Parking() { }
		public static Parking getParking()
		{
			if (parking == null)
				parking = new Parking();
			return parking;
		}
	}
}