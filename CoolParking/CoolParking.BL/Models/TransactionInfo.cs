﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

// Процес оплати фіксується Транзакцією. У ній вказується час Транзакції, 
// ідентифікатор Тр. засобу та кількість грошей, які були списані з нього. 
using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
		public TransactionInfo(string id, decimal sum, DateTime time)
		{
			this.Id = id;
			this.Sum = sum;
			this.Time = time;
		}
		public string Id { get; }
		public decimal Sum { get; }
		public DateTime Time { get; }
		public override string ToString()
		{
			return $"{Id} {Sum} {Time}";
		}

	}
}
