﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.IO;

namespace CoolParking.BL.Services
{
	public class LogService : ILogService
	{
		private string logPath;
		public LogService (string logPath)
		{
			this.logPath = logPath;
		}
		public string LogPath { get => logPath; set => logPath = value; }
		public string Read()
		{
			try
			{
				using (StreamReader sr = new StreamReader(logPath))
				{
					return sr.ReadToEnd();
				}
			}
			catch (FileNotFoundException)
			{
				throw new InvalidOperationException($"File {LogPath} not found");
			}
		}
		public void Write(string logInfo)
		{
			lock (Settings.fileLocker)
			{
				using (StreamWriter sw = new StreamWriter(logPath, true, System.Text.Encoding.Default))
				{
					sw.WriteLine(logInfo);
				}
			}
		}
	}
}